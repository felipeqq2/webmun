// Implements a simple, custom logging package.
package log

import (
	"fmt"
	"os"
	"time"

	"codeberg.org/felipeqq2/webmun/helpers"
	"github.com/fatih/color"
)

type Logger struct {
	debug bool
}

// Create a new [Logger] instance
func New(config *helpers.Config) Logger {
	return Logger{debug: config.Debug}
}

// Logs the message to stderr, and exits with code 1.
func (l Logger) Exit(message interface{}) {
	l.Elog(message)
	os.Exit(1)
}

// Logs message to stderr.
//
// If the --debug flag is enabled, adds a timestamp.
func (l Logger) Elog(message interface{}) {
	red := color.New(color.FgHiRed).SprintFunc()
	if l.debug {
		timeDebug(red(message), true)
	} else {
		fmt.Fprintln(os.Stderr, red(message))
	}
}

// If the --debug flag is enabled, logs message to stderr with a timestamp.
func (l Logger) Elogd(message interface{}) {
	if l.debug {
		timeDebug(message, true)
	}
}

// Logs message to stdout.
//
// If the --debug flag is enabled, adds a timestamp.
func (l Logger) Log(message interface{}) {
	if l.debug {
		timeDebug(message, false)
	} else {
		fmt.Println(message)
	}
}

// Print message with timestamp
func timeDebug(message interface{}, stderr bool) {
	yellow := color.New(color.FgYellow).SprintFunc()
	timestamp := yellow(time.Now().Format("2006-01-02 15:04:05"))
	if stderr {
		fmt.Fprintln(os.Stderr, timestamp, message)
	} else {
		fmt.Println(timestamp, message)
	}
}
