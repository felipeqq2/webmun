package main

import (
	"embed"
	"errors"
	"io/fs"
	"os"
	"path"
	"strings"

	"codeberg.org/felipeqq2/webmun/log"
)

//go:embed example/*
var example embed.FS

func initialize(l *log.Logger, dir string) {
	entries, err := os.ReadDir(dir)
	if err != nil && !errors.Is(err, os.ErrNotExist) {
		l.Exit(err)
	}
	if errors.Is(err, os.ErrNotExist) { // create directory if it doesn't exist
		if err := os.MkdirAll(dir, 0755); err != nil {
			l.Exit(err)
		}
	}
	if len(entries) != 0 {
		l.Exit("Error: directory is not empty.")
	}

	files, err := example.ReadDir("example")
	if err != nil {
		l.Exit(err)
	}

	copyDir(l, files, "example", dir)
}

func copyDir(l *log.Logger, files []fs.DirEntry, fsh string, dir string) {
	for _, file := range files {
		fileAbs := path.Join(fsh, file.Name())
		actualFsh := strings.Join(strings.Split(fileAbs, "/")[1:], "/")

		if file.IsDir() {
			fsdir, err := example.ReadDir(fileAbs)
			if err != nil {
				l.Exit(err)
			}

			// create dir
			if err := os.MkdirAll(path.Join(dir, actualFsh), 0755); err != nil {
				l.Exit(err)
			}

			copyDir(l, fsdir, path.Join(fsh, file.Name()), dir)
		} else {
			c, err := example.ReadFile(fileAbs)
			if err != nil {
				l.Exit(err)
			}

			if err := os.WriteFile(path.Join(dir, actualFsh), c, 0644); err != nil {
				l.Exit(err)
			}
		}
	}
}
