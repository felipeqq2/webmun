package main

import (
	"fmt"
	"os"
	"path"

	"codeberg.org/felipeqq2/webmun/helpers"
	"codeberg.org/felipeqq2/webmun/log"
	"codeberg.org/felipeqq2/webmun/web"
)

func main() {
	config := helpers.ParseConfig()
	l := log.New(&config)

	l.Elogd(fmt.Sprintf("Loaded config: %+v", config))

	if config.Help {
		helpers.PrintHelp()
		os.Exit(0)
	}

	if config.Version {
		helpers.PrintVersion()
		os.Exit(0)
	}

	switch config.Command {
	case helpers.Init:
		l.Log(fmt.Sprintf("Initializing new project at %s/", path.Clean(config.Directory)))
		l.Log("Using default values.")
		initialize(&l, config.Directory)
		l.Log("Successfully initialized!")

	case helpers.Generate:
		l.Log(fmt.Sprintf("Generating website at %s/", path.Join(config.Directory, "dist")))
		web.Generate(&l, config.Directory)
		l.Log("Successfully generated!")

	case helpers.Serve:
		l.Log(fmt.Sprintf("Serving and watching website at %s/", path.Join(config.Directory, "dist")))
		watch(&l, config.Directory, config.Port)

	default:
		l.Elog("Unknown command")
		helpers.PrintHelp()
	}
}
