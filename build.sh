#!/usr/bin/env sh

if [ -z "$1" ]; then
    echo "Run: ./build.sh <version>"
    exit 1
fi

gox -os "windows linux" -arch "arm amd64 arm64 386" \
    -ldflags "-X 'codeberg.org/felipeqq2/webmun/helpers.version=$1'" \
    -output "./out/{{.Dir}}_$1_{{.OS}}_{{.Arch}}"
