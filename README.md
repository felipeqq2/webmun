# WebMUN generator
1. Run `webmun init` to initialize a new project at the current directory;
2. Modify generated files (`webmun.yaml`, posts and assets) according to your specific needs;
3. Run `webmun generate` to generate a brand-new static website at `./dist`;
4. :rainbow:

## To-do
- [ ] Add `feed.xml`;
- [ ] Add sitemap;

---

This is a new version of a boilerplate repo built with [SvelteKit](https://github.com/sveltejs/kit). It worked ~OK, until things started to break with their updates (they were on alpha at the time). As it turns out, such a powerful framework also adds much unnecessary bloat and complexity to the application, delivering massive JS files (although much more performant if compared to others) and offering little benefits for such a small use case.

For now, unfortunately I'll lose features such as SCSS, Typescript and automatic JS/CSS bundling and processing, but it's a small price to pay for salvation (and I can always add them later if I really need to; adding new features just became much easier with this new setup).

I'm using _two_ lines of JS for the whole website. I don't need such a framework _at all_.
