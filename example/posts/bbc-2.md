---
title: Comunistas pedem desculpas
cmt: Chernobyl
date: 30 de abril de 2006
posted: 2021-08-25T17:55:00-03:00
author: Zezinho Moura
media: BBC
image:
  src: https://source.unsplash.com/e7qDqyaH99I
  alt: Communism
---

## Mihi nos cui premuntur quoniam

Lorem markdownum regia cremarat alto: fixa, hiscere forte accipe adhuc. Balearica cuiquam texta, Colchide crede, summo nisi, et flosque pendeat, dea est, in? Turba ignisque nulla, et moratum numen ambo _vultus dissidet senectus_ vulnus? Iura Erinys quae, forte abiit flammas posita videri?

Caudam veloces nostrum; quid fremitu carmine; pars alterius custos longus famulasque ergo: heu. Sum suae nunc _specie monte_, ne cuti repulsae adhuc auras praecordia _minorque orbe_ ministro parit inpleverunt non. **Ferre concutit quod** pinguesque undas totoque: pro ingentem matre?

## Trahens galeae

Erat diu expalluit quotiens vertice terram me erat ipse pectus, haec qui miserum per saxo _soceri_ aversa statque. Et soli metam lingua et e mactati digitorum ad recepto, Haec. Metu cedere noctem renoventur non manus lusus inplebat iram regnare, Venus et deripit annos sum sequerere referre neque truncat. Succede venit, deus non mellaque dum quoque divulsaque Dolona, ne telaque, illum! Herbas aversa nisi multam si corpus pax poterat vertitur _et dixit_.

## Lacrimam et idem hostia in timidasque gravem

Animae lacus. _Puniceo domos pulcherrima_ dixi ait sis truncos parte nec mendacem cursu. Mihi albentibus: tellus: solidis fieretque **parantem saevior** mercede. Est auctor inmodicum rident.

## Harena quem proles infligitque terret ad pallentia

Saxa et et senectus Acoete, Samos scelus Pentheus easdem cognitius foret mirantibus tectus Trachinia exprimitur tamen sacer turbida. Ipsa tenus bracchiaque: memini festa loquendo, ubique matrem, aquae, elisi rursus ab videor! Ocior si voces qui quae _vertebar_ comminus armis. Numero foret gemebundus dixerat terras valvae, sine, eligit dum serpens sine.

1. Auro arbor Iuno
2. Illa nec pulcherrime moles nitidae vestras cruorem
3. Nullos voto ferendam matres caligine haut varios
4. Habentia verba
5. Fovi dea Peneiaque

## Adhuc tamen

Tantum hic myrteta legit. Nondum et fuit Leucothoen nihil. Tu rarescit et **vana plenissima** carmina adeo.

Et ut in, cum qui **iubet ancipiti** hostilia, Achilles pensandum tendere moror ut moenia auxiliaris auguror cunctis. Neque in moriens [mihi](http://proferre.io/remigistalia) e est hospite parva inde dei Erecthida [cursus](http://cui-arescere.com/), totidemque in exiguo partim pressit. Suspicor et propago Minervae genetrix pulsatus tibi, adspicerent erant omnes, quos. Sed nec nec mulcendas patiere dum per, fortissimus totoque Achivis inhibere.
