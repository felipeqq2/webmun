package helpers

import (
	"fmt"
	"os"
	"runtime"
)

// ldflags
var version = "dev"

// PrintHelp prints version information to STDOUT
func PrintHelp() {
	PrintVersion()

	m := `
Usage:
  webmun <flags> [command]

Commands:
  init       initialize new project at the current directory
  generate   generate new website files at ./dist
  serve      serve the website generated files, while watching for changes

Flags:
  -h, --help      print help information
  -d, --debug     enable debug mode
  -v, --version   print version information
  --dir <dir>     project directory
  --port <port>   port to serve the website (default: 8090)`

	fmt.Fprintln(os.Stderr, m)
}

// PrintVersion prints version information to STDOUT
func PrintVersion() {
	fmt.Fprintf(os.Stderr, "WebMUN %v (%v, %v/%v)\n", version, runtime.Version(), runtime.GOOS, runtime.GOARCH)
}
