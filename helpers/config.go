package helpers

import (
	"flag"
	"os"
	"path"
	"strings"
)

type Command int

const (
	// Initialize a new project.
	Init Command = iota
	// Generate the website files.
	Generate
	// Serve and watch the website files.
	Serve
	Unknown
)

type Config struct {
	Version   bool
	Debug     bool
	Help      bool
	Directory string
	Port      string
	Command   Command
}

func ParseConfig() Config {
	var (
		version   bool
		debug     bool
		help      bool
		directory string
		port      string
	)

	flag.BoolVar(&version, "version", false, "display version info")
	flag.BoolVar(&version, "v", false, "display version info")

	flag.BoolVar(&debug, "debug", false, "enable debug mode")
	flag.BoolVar(&debug, "d", false, "enable debug mode")

	flag.BoolVar(&help, "help", false, "display help info")
	flag.BoolVar(&help, "h", false, "display help info")

	flag.StringVar(&directory, "dir", ".", "project directory")
	flag.StringVar(&port, "port", "8090", "port to serve the website")

	// print help info on error
	flag.Usage = func() { PrintHelp() }

	flag.Parse()

	wd, _ := os.Getwd()

	return Config{
		Version:   version,
		Debug:     debug,
		Help:      help,
		Directory: path.Join(wd, directory),
		Port:      port,
		Command:   getCommand(flag.Arg(0)),
	}
}

func getCommand(arg string) Command {
	switch strings.ToLower(arg) {
	case "init":
		return Init
	case "generate":
		return Generate
	case "serve":
		return Serve
	default:
		return Unknown
	}
}
