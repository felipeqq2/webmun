package main

import (
	"net/http"
	"path"
	"strings"

	"codeberg.org/felipeqq2/webmun/log"
	"codeberg.org/felipeqq2/webmun/web"
	"github.com/fsnotify/fsnotify"
)

// Run static file server
func serve(l *log.Logger, dir string, port string) {
	fsHandler := http.FileServer(http.Dir(path.Join(dir, "/dist/")))
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		l.Elogd(r.Method + " request at " + r.URL.Path)

		hasExt := path.Ext(r.URL.Path) != ""
		trailingSlash := strings.HasSuffix(r.URL.Path, "/")
		isIndex := r.URL.Path == "/" // index.html is automatically handled by http.FileServer

		// add trailing .html if we're looking for an html file
		if !hasExt && !isIndex {
			// remove trailing slash, if present
			if trailingSlash {
				r.URL.Path = r.URL.Path[:len(r.URL.Path)-1]
			}
			r.URL.Path += ".html"
		}

		fsHandler.ServeHTTP(w, r)
	})

	l.Log("Serving website at http://localhost:" + port)
	if err := http.ListenAndServe(":"+port, nil); err != nil {
		l.Log(err)
	}
}

// Watch directory, rebuild when any file changes, and serve it statically
func watch(l *log.Logger, dir string, port string) {
	w, err := fsnotify.NewWatcher()
	if err != nil {
		l.Exit(err)
	}
	defer w.Close()

	innerFolders := []string{"/", "/assets", "/posts"}

	for _, inner := range innerFolders {
		if err := w.Add(path.Join(dir, inner)); err != nil {
			l.Exit(err)
		}
	}

	go func() {
		for {
			select {
			case event := <-w.Events:
				if !strings.Contains(event.Name, "/dist") {
					l.Log("File changed: regenerating website.")
					web.Generate(l, dir)
				}
			case err := <-w.Errors:
				l.Exit(err)
			}
		}
	}()

	web.Generate(l, dir)

	serve(l, dir, port)
}
