module codeberg.org/felipeqq2/webmun

go 1.17

require (
	github.com/Machiel/slugify v1.0.1
	github.com/adrg/frontmatter v0.2.0
	github.com/fatih/color v1.14.1
	github.com/fsnotify/fsnotify v1.6.0
	github.com/gomarkdown/markdown v0.0.0-20221013030248-663e2500819c
	github.com/otiai10/copy v1.9.0
	gopkg.in/yaml.v3 v3.0.1
)

require (
	github.com/BurntSushi/toml v0.3.1 // indirect
	github.com/mattn/go-colorable v0.1.13 // indirect
	github.com/mattn/go-isatty v0.0.17 // indirect
	golang.org/x/sys v0.3.0 // indirect
	gopkg.in/yaml.v2 v2.3.0 // indirect
)
