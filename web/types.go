package web

import (
	"html/template"
	"time"
)

type website struct {
	Name         string
	Vendor       string
	Url          string
	Description  string
	Subscription string
	Hero         image
	Guides       guides
	Committees   []committee
	Medias       []media
	Menu         []menuItem
	Style        style
	Posts        []post
	Page         page // only for internal use, specific page configuration
}

// Page-specific configuration
type page struct {
	Title string
	Image string
	Plain bool   // disable header and footer
	Index int    // indexable things
	Posts []post //specific posts
}

type guides struct {
	Rules string
	Docs  string
}

type committee struct {
	Name        string
	Description string
	Text        string
	Images      []image
	Guide       string
	Reps        string
	IsPress     bool
}

type media struct {
	Name    string
	Image   image
	Authors string
}

type menuItem struct {
	Link string
	Text string
}

type image struct {
	Src string
	Alt string
}

type style struct {
	Header      []string
	Body        []string
	ImportFonts []string

	Primary   color
	Secondary color
	Link      color
}

type color [3]uint8

type post struct {
	Title   string
	Cmt     string
	Posted  time.Time
	Date    string
	Author  string
	Media   string
	Image   image
	Content template.HTML
}
