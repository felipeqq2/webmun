// Process and generate final web files (HTML, CSS, JS).
package web

import (
	"fmt"
	"html/template"
	"io/ioutil"
	"os"
	"path"
	"sort"
	"strings"

	"codeberg.org/felipeqq2/webmun/log"
	"github.com/adrg/frontmatter"
	"github.com/gomarkdown/markdown"
	"gopkg.in/yaml.v3"

	cp "github.com/otiai10/copy"
)

// Generate website based on the selected directory configuration
func Generate(l *log.Logger, directory string) {
	dist := path.Join(directory, "/dist/")

	// clean /dist/ directory
	l.Elogd("Deleting outdated files at " + dist)
	if err := os.RemoveAll(dist); err != nil {
		l.Exit(err)
	}

	websiteConf, err := getConfig(l, directory)
	if err != nil {
		l.Exit(err)
	}

	files := process(websiteConf, l)

	for _, file := range files {
		// mkdir parent directories
		if err := os.MkdirAll(path.Join(dist, path.Dir(file.path)), 0755); err != nil {
			l.Exit(err)
		}

		// write file
		if err := ioutil.WriteFile(path.Join(directory, "/dist/", file.path), []byte(file.contents), 0644); err != nil {
			l.Exit(err)
		}
	}

	if err = cp.Copy(path.Join(directory, "/assets/"), dist); err != nil {
		l.Exit(err)
	}
}

func getConfig(l *log.Logger, directory string) (website, error) {
	data, err := os.ReadFile(path.Join(directory, "./webmun.yaml"))
	if err != nil {
		return website{}, err
	}

	configFile := string(data)

	// defaults
	websiteConf := website{}

	err = yaml.Unmarshal([]byte(configFile), &websiteConf)
	if err != nil {
		return website{}, err
	}

	l.Elogd(fmt.Sprintf("Website configuration (as fetched from webmun.yaml):\n%+v)", websiteConf))

	// fetch posts
	var posts []post

	postDir := path.Join(directory, "/posts/")
	postFiles, err := os.ReadDir(postDir)
	if err != nil {
		return website{}, err
	}

	for _, f := range postFiles {
		if !f.IsDir() && path.Ext(f.Name()) == ".md" {
			data, err := os.ReadFile(path.Join(postDir, f.Name()))
			if err != nil {
				return website{}, err
			}

			postContent := string(data)

			var current post
			rest, err := frontmatter.Parse(strings.NewReader(postContent), &current)
			if err != nil {
				return website{}, err
			}

			current.Content = template.HTML(markdown.ToHTML(rest, nil, nil))

			posts = append(posts, current)
		}
	}

	// Sort posts reverse-chronologically
	sort.Slice(posts, func(i, j int) bool {
		return posts[i].Posted.After(posts[j].Posted)
	})

	websiteConf.Posts = posts

	l.Elogd(fmt.Sprintf("Posts:\n%+v", posts))

	return websiteConf, nil
}

type file struct {
	path     string
	contents string
}
