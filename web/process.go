package web

import (
	"bytes"
	"embed"
	"fmt"
	"html/template"
	"math"
	"path"
	"strings"
	"time"

	"codeberg.org/felipeqq2/webmun/log"

	"github.com/Machiel/slugify"
)

//go:embed assets/* components/*
var files embed.FS

// Process template by name, according to conf
func processTemplate(name string, conf website, l *log.Logger) string {
	tmpl, err := template.New("").
		Funcs(template.FuncMap{
			"join":      strings.Join,
			"now":       time.Now,
			"slugify":   slugify.Slugify,
			"color":     func(c color) string { return fmt.Sprintf("rgb(%v, %v, %v)", c[0], c[1], c[2]) },
			"textColor": textColor,
			"rgba":      func(c color, a string) string { return fmt.Sprintf("rgba(%v, %v, %v, %v)", c[0], c[1], c[2], a) },
		}).
		ParseFS(files, "components/*", path.Join("assets/", name)) // parse components and specific template
	if err != nil {
		l.Exit(err)
	}

	l.Elogd(fmt.Sprintf("Processing %s - %s", name, conf.Page.Title))

	var tmp bytes.Buffer

	_, f := path.Split(name) // ExecuteTemplate asks only for file name
	if err := tmpl.ExecuteTemplate(&tmp, f, conf); err != nil {
		l.Exit(err)
	}

	return tmp.String()
}

func process(conf website, l *log.Logger) []file {
	// Helper function to generate [file] with default [page]
	mini := func(r string) file {
		return file{path: r, contents: processTemplate(r, mergePage(conf, page{Title: conf.Name}), l)}
	}

	// Helper function to generate [file] with custom [page]
	miniP := func(r string, p page) file {
		return file{path: r, contents: processTemplate(r, mergePage(conf, p), l)}
	}

	return concatFiles([][]file{
		{
			mini("styles.css"),
			mini("script.js"),
			miniP("404.html", page{Title: "Página não encontrada - Erro 404", Plain: true}),
			mini("index.html"),
		},
		generateCommittees(conf, l),
		generateAc(conf, l),
	})
}

func generateAc(conf website, l *log.Logger) []file {
	indexPage := page{
		Title: "Notícias - " + conf.Name,
		Image: getPressCmt(conf.Committees, l).Images[0].Src,
		Posts: conf.Posts,
	}

	mediaPage := page{Title: "Confira nossos jornais!"}

	final := []file{{
		path:     "ac.html",
		contents: processTemplate("ac.html", mergePage(conf, indexPage), l),
	}, {
		path:     "ac/m.html",
		contents: processTemplate("ac/m.html", mergePage(conf, mediaPage), l),
	}}

	for i, cmt := range conf.Committees {
		p := page{
			Title: "Notícias - " + cmt.Name,
			Image: cmt.Images[0].Src,
			Index: i,
			Posts: filterPosts(conf.Posts, func(p post) bool { return p.Cmt == cmt.Name }),
		}
		new := file{
			path:     fmt.Sprintf("/ac/%s.html", slugify.Slugify(cmt.Name)),
			contents: processTemplate("/ac/cmt.html", mergePage(conf, p), l),
		}
		final = append(final, new)
	}

	for i, media := range conf.Medias {
		p := page{
			Title: "Notícias - " + media.Name,
			Image: media.Image.Src,
			Index: i,
			Posts: filterPosts(conf.Posts, func(p post) bool { return p.Media == media.Name }),
		}

		new := file{
			path:     fmt.Sprintf("/ac/m/%s.html", slugify.Slugify(media.Name)),
			contents: processTemplate("ac/im.html", mergePage(conf, p), l),
		}

		final = append(final, new)
	}

	for i, post := range conf.Posts {
		p := page{
			Title: post.Title,
			Image: post.Image.Src,
			Index: i,
		}

		new := file{
			path:     fmt.Sprintf("/ac/m/%s/%s.html", slugify.Slugify(post.Media), slugify.Slugify(post.Title)),
			contents: processTemplate("ac/post.html", mergePage(conf, p), l),
		}

		final = append(final, new)
	}

	return final
}

func generateCommittees(conf website, l *log.Logger) []file {
	var final []file

	for i, cmt := range conf.Committees {
		p := page{Title: cmt.Name, Image: cmt.Images[0].Src, Index: i}
		new := file{
			path:     fmt.Sprintf("/c/%s.html", slugify.Slugify(cmt.Name)),
			contents: processTemplate("/c/cmt.html", mergePage(conf, p), l),
		}
		final = append(final, new)
	}

	return final
}

// Get proper text color according to provided background color luminance. Can be either white or black.
func textColor(c color) color {
	// see https://stackoverflow.com/a/3943023
	calc := func(i float64) float64 {
		if i <= 0.04045 {
			return i / 12.92
		} else {
			return math.Pow((i+0.055)/1.055, 2.4)
		}
	}

	// see https://stackoverflow.com/a/3943023
	r := float64(c[0]) / 255
	g := float64(c[1]) / 255
	b := float64(c[2]) / 255

	luminance := 0.2126*calc(r) + 0.7152*calc(g) + 0.0722*calc(b)

	if luminance > 0.179 {
		return color{0, 0, 0}
	} else {
		return color{255, 255, 255}
	}
}
