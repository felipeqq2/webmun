package web

import "codeberg.org/felipeqq2/webmun/log"

// Get Press committee
func getPressCmt(cmt []committee, l *log.Logger) committee {
	var (
		pressCmt   committee
		foundPress bool
	)
	for _, cmt := range cmt {
		if cmt.IsPress {
			pressCmt = cmt
			foundPress = true
			break
		}
	}

	if !foundPress {
		l.Exit("Couldn't find AC committee")
	}

	return pressCmt
}

// Merge [website] and custom [page]
func mergePage(a website, b page) website {
	a.Page = b
	return a
}

func concatFiles(slices [][]file) []file {
	var totalLen int

	for _, s := range slices {
		totalLen += len(s)
	}

	result := make([]file, totalLen)

	var i int
	for _, s := range slices {
		i += copy(result[i:], s)
	}

	return result
}

// see https://stackoverflow.com/a/37563128
func filterPosts(ss []post, test func(post) bool) (ret []post) {
	for _, s := range ss {
		if test(s) {
			ret = append(ret, s)
		}
	}
	return
}
